import React, { Component } from 'react';
import { Input, Button } from 'mdbreact';

class InputBid extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      price: null
    };
  }

  getValidationState() {
    const { startingPrice, topPrice } = this.props;
    const { price } = this.state;

    if (price) {
      if (topPrice > 0) {
        if (price > topPrice) {
          return false;
        } else {
          return true;
        }
      } else {
        if (price >= startingPrice) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  onSubmit(e) {
    e.preventDefault();

    const { productId } = this.props;
    const { price } = this.state;

    this.props.onBid(productId, price);
    this.setState({ price: null });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { price } = this.state;

    return (
      <form onSubmit={e => this.onSubmit(e)}>
        <Input
          type="number"
          name="price"
          label="Giá thầu"
          value={price}
          min={100}
          onChange={e => this.onChange(e)}
        />
        <Button size="sm" type="submit" disabled={this.getValidationState()}>
          OK
        </Button>
      </form>
    );
  }
}

export default InputBid;
