import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'mdbreact';
import { Query, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import DateTimePicker from 'react-datetime-picker';
import Select from 'react-select';
import { toast } from 'react-toastify';

const FIND_MANY_LOCATION_QUERY = gql`
  query {
    findManyLocation {
      _id
      name
    }
  }
`;

const CREATE_PRODUCT_MUTATION = gql`
  mutation CreateProduct(
    $dateFrom: Date!
    $dateTo: Date!
    $locationId: ID!
    $startingPrice: Int!
  ) {
    createProduct(
      dateFrom: $dateFrom
      dateTo: $dateTo
      locationId: $locationId
      startingPrice: $startingPrice
    ) {
      _id
      locationId {
        name
        value
      }
      startingPrice
      firstPrice {
        advertiserId {
          name
        }
        price
      }
      dateFrom
      dateTo
      status
    }
  }
`;

class CreateProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      dateFrom: new Date(),
      dateTo: new Date(),
      price: 0,
      locationId: ''
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  onCreateProduct = async e => {
    e.preventDefault();

    const { dateFrom, dateTo, price, locationId } = this.state;

    const result = await this.props.client.mutate({
      mutation: CREATE_PRODUCT_MUTATION,
      variables: {
        dateFrom: dateFrom,
        dateTo: dateTo,
        startingPrice: parseFloat(price),
        locationId: locationId
      },
      errorPolicy: 'all'
    });

    if (!result.data.createProduct) {
      toast.error(result.errors[0].message);
    } else {
      toast.success('Tạo sản phẩm thành công!');

      this.setState({
        modal: false,
        dateFrom: new Date(),
        dateTo: new Date(),
        price: 0,
        locationId: ''
      });
    }
  };

  render() {
    const { dateFrom, dateTo, selectedOption } = this.state;

    return (
      <div>
        <a onClick={this.toggle}>Tạo</a>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Tạo mới</ModalHeader>
          <ModalBody>
            <div className="container">
              <div className="row">
                <div className="col">
                  <div>
                    <label>Địa điểm</label>
                    <Query query={FIND_MANY_LOCATION_QUERY}>
                      {({ loading, error, data }) => {
                        if (loading) return <div>Loading</div>;
                        if (error) return <div>Error</div>;
                        const { findManyLocation } = data;
                        const options = findManyLocation.map(item => ({
                          value: item._id,
                          label: item.name
                        }));
                        return (
                          <Select
                            placeholder=""
                            value={selectedOption}
                            onChange={selectedOption =>
                              this.setState({
                                selectedOption,
                                locationId: selectedOption.value
                              })
                            }
                            options={options}
                          />
                        );
                      }}
                    </Query>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div>
                    <label>Giá khởi điểm</label>
                    <input
                      name="price"
                      type="number"
                      className="form-control"
                      onChange={e => this.setState({ price: e.target.value })}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div>
                    <label>Từ</label>
                    <DateTimePicker
                      onChange={date => this.setState({ dateFrom: date })}
                      value={dateFrom}
                    />
                  </div>
                </div>
                <div className="col-6">
                  <div>
                    <label>Đến</label>
                    <DateTimePicker
                      onChange={date => this.setState({ dateTo: date })}
                      value={dateTo}
                    />
                  </div>
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button onClick={e => this.onCreateProduct(e)}>OK</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default withApollo(CreateProduct);
