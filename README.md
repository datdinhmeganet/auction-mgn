
# Sơ lược
 **Mục tiêu?**

- Tăng lợi nhuận cho ta.

>Ví dụ: vị trí Vincom Q.1 không có đấu giá thì ta bán giá 10tr. Khi đấu giá,  ta bán được vị trí Vincom Q.1 với 12tr do có nhiều khách hàng tranh nhau mua. Lợi nhuận tăng 2 triệu.

- Cho khách hàng trải nghiệm mua quảng cáo nhanh, mua được hàng mình muốn với đúng giá hơn.

**Tầm nhìn**

- Dự án đóng góp vào sản phẩm lớn: *sàn* giao dịch quảng cáo MegaNet.

**Dự án là gì?**

- Hệ thống đấu giá quảng cáo theo cơ chế nhì giá.

**Dự định ngắn hạn**

- Được Tổng Giám đốc Lê Minh Châu và trưởng phòng Nguyễn Hữu Tùng duyệt triển khai.

**Dự định trung hạn**

- 1 tháng: sử dụng dữ liệu thật và lôgíc thật, ra sản phẩm khả thi tối thiểu (MVP).

- 2 tháng: tích hợp ứng dụng KPI và đặt hàng của developer N.Q. Học, thành 1 sản phẩm đồng bộ.

**Dự định dài hạn**

- Tối đa 3 tháng có sản phẩm hoàn chỉnh có thể đưa cho khách hàng sử dụng để  nhanh lấy phản hồi nhanh để nhanh lặp lại quy trình nhanh hoàn thiện nhanh sản phẩm.

# Chi tiết triển khai

Ta sẽ cho đấu giá sử dụng biến thể của cơ chế **nhì giá**, TA: *Generalized Second Price - GSP*.

- Lý do: Cơ chế nhì giá khuyến khích cho người tham gia đấu giá đưa ra mức giá bằng với giá trị thật mà người tham gia cho là phù hợp với món hàng.

- Cụ thể:

<!---Có `k` người tham gia đấu giá, mỗi người ra giá b<sub>k</sub>.
--->
 **Quan trọng: _Người tham gia không được biết và cũng không cần biết giá người khác đưa ra._**
<!--- 
Có `i` vị trí quảng cáo. `i` càng nhỏ thì vị trí quảng cáo càng có giá trị.


**Tiến hành đấu giá:**

1. Sắp xếp các mức giá được nhận thành b<sub>1</sub> > b<sub>2</sub> > b<sub>3</sub> > ... > b<sub>k</sub>.

2. Sắp xếp vị trí `i` cho người ra giá b<sub>i</sub>.

3. Tính mức giá b<sub>i+1</sub> người ra giá b<sub>i</sub>.

--->
Vì người ra giá cao nhất chỉ phải trả mức giá cao thứ nhì nên gọi là cơ chế **nhì giá**.

**Ví dụ:**
>Có 2 vị trí quảng cáo là Vincom Q.1 và Landmark 81 Q.Bình Thạnh; Vincom có giá trị cao hơn Landmark 81. 
>
>Có 4 người tham gia đấu giá đưa ra mức giá lần lược là 15, 14, 7, 3 triệu.
>
>Ta sẽ bán Vincom cho người ra giá 15 triệu và bán với giá 14 triệu, và bán Landmark 81 cho người ra giá 14 triệu nhưng bán với giá 7 triệu.

### Hơn thua so với đấu giá truyền thống?

Vincom khởi điểm đấu giá 10tr. 

Có khách A trả giá 12tr. Khách B không biết khách A trả bao nhiêu hoặc thậm chí có khách A hay không. 

Giá trị thực (hoặc khả năng chi trả) mà khách B nghĩ Vincom đáng giá là 15tr. Nếu khách B mua được Vincom với giá 10tr, khách B "lời" 5tr; với giá 12tr thì "lời" 3tr, v.v. 

Khách B sẵn sàng chi trả bất cứ giá nào dưới 15 triệu - nếu mua được dưới 15tr tức là khách B lời.

Tuy nhiên với đấu giá truyền thống thì khách B chỉ muốn ra giá và trả tiền cao hơn người thứ nhì 1 nghìn đồng, vì như vậy là khách B đã có thể thắng được món hàng mà lại còn được trả ít tiền nhất.

Tức là để làm được điều này thì khách B sẽ cân bằng việc tiết kiệm tiền bằng cách ra giá thấp hơn với rủi ro bị thua cuộc (thua cuộc dưới 15tr tức là khách B lỗ).

Nói cách khác **khách B sẽ không muốn đưa ra giá trị thực** mà khách B cho là đúng với món hàng của ta - tức là 15tr.

Trong cơ chế nhì giá thì thực chất ta đang nói với khách B là "chị hãy ra giá trị thực của chị đi" - tức là 15tr - "chúng tôi sẽ chỉ tính tiền chị với giá của người thứ nhì thôi. Chị đỡ phải suy tính rủi ro có thua cuộc hay không."

Như thế cơ chế nhì giá khuyến khích **khách B đưa ra giá trị thực** mà khách B cho là đúng - thay vì phải giấu nó đi để đưa ra giá thấp hơn và cân bằng rủi ro. 

Trải nghiệm của khách sẽ tốt hơn. Ngay lần đầu tiên khách sẽ chỉ cần ra giá bằng giá trị thật mà khách gắn với món hàng là xong - không cần phải so đo tính toán và chầu chực đợi có ai đưa ra giá cao hơn mình không.

Nếu ta áp dụng logic này với toàn bộ khách đấu giá, thì ta sẽ có một cuộc đấu giá mà tất cả người tham gia đều đưa ra giá trị thực. Từ đó ta sẽ nắm được thông tin quan trọng về giá trị của món hàng mà ta đang rao bán và phân bổ nguồn lực cho những món hàng đó cũng như đề xuất giá khởi điểm cao hơn cho lần sau.

Tuy nhiên có một điểm trừ lớn: với đấu giá truyền thống, khách B có thể đã đặt 15tr (và chấp nhận không có "lời") và ta sẽ nhận được 15tr đó - nhưng với đấu giá nhì giá thì ta chỉ nhận được giá cao thứ nhì (tức là 13tr hoặc 14tr mà thôi). 

Ta sẽ bị mất đi khoảng thu vài triệu đó trong thời gian đầu khi ta chưa nắm rõ được giá trị món hàng của ta (và vì vậy chưa chặn giá khởi điểm phù hợp). 

Tác giả nhận thấy điểm trừ này có thể được khắc phục, và điểm cộng lớn khi khách hàng vui vẻ cũng như ta có nhiều thông tin *thực* về sản phẩm và khách hàng hơn có thể dùng để cải tiến sản phẩm trong tương lai, và vì vậy quyết định rõ ràng là cần phải dùng cơ chế nhì giá.

### Tại sao gọi là "biến thể"? Chỉnh sửa cái gì?

Có một vài chỉnh sửa trên cơ chế này để đảm bảo cơ chế  có lợi cho ta, và cũng tính đến thực tiễn áp dụng vào môi trường Việt Nam.

- Tác giả dự tính cần phải có thời gian để khách hàng Việt Nam chưa sử dụng cơ chế đấu giá này bao giờ sẽ bị bỡ ngỡ hoặc chưa thật sự hiểu cách đấu giá. Trong thời gian ứng dụng sẽ mở những lựa chọn nhất định làm cho cơ chế nhì giá giống với cơ chế  truyền thống.

>Những lựa chọn này trước bắt bao gồm việc khách hàng vẫn có thể xem giá người khác đặt và điều chỉnh giá mình đặt. Điều này không được làm trong cơ chế nhì giá đúng.

- Bắt buộc giá khởi điểm để đảm bảo những người tham gia đấu giá không thông đồng với nhau và bảo đảm doanh thu tối thiểu cho ta.

> Ví dụ: vị trí Vincom không có đấu giá thì ta bán giá 10tr. Tức là có tối thiểu 1 khách hàng đồng ý trả giá 10tr. Với đấu giá, ta bắt buộc giá khởi điểm là 10tr. 
>
>Nếu hàng của ta chỉ có 1 khách mua thì ta sẽ vẫn bán được với giá 10tr, tức là trong tình huống xấu nhất đấu giá vẫn đảm bảo doanh thu không thấp hơn so với không đấu giá.

[Bài viết](https://theory.stanford.edu/~tim/f16/l/l15.pdf) của ĐH Stanford.

[Bài viết](https://www.linkedin.com/pulse/why-do-second-price-auctions-work-chetan-prabhu) trên LinkedIn so sánh đơn giản sự khác biệt giữa cơ chế nhì giá và cơ chế truyền thống.

- Server: GraphQL, client: ReactJS.

#### Xem xét trong tương lai

- Ở một lần đăng nhập sẽ có nhiều vị trí quảng cáo hoặc có nhiều trang đăng nhập hoặc slider nhiều hình (hình chạy từ trái qua phải).

- Trong tương lai ta có thể áp dụng thêm hình thức đấu giá VCG, với một chút chỉnh sửa thì VCG có nhiều mặt lợi, tuy nhiên với tình hình hiện thay thì chưa nên và cũng chưa thể áp dụng hình thức đấu giá này.

# Tác giả
- Đinh Công Đạt: khởi động dự án, viết server và lôgíc đấu giá.
- Phạm Dũ Phong: viết server, xác thực người dùng, hoàn thiện lôgíc đấu giá và client.
- Lê Xuân Nam: viết đăng nhập cho client.

## Cột mốc 

- Ngày 05/08/2018: khởi động dự án
- 08/08/2018: hoàn thành vật mẫu cho server. 
- 10/08/2018: hoàn thành client.


