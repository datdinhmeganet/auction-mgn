const { GraphQLServer } = require('graphql-yoga');
const mongoose = require('mongoose');
const cron = require('node-cron');
const schema = require('./schema/schema');
const { DATABASE } = require('./config/config');
const Product = require('./models/product');
const Bid = require('./models/bid');
const Advertiser = require('./models/advertiser');
const pubsub = require('./utils/subscriptions');

process.setMaxListeners(Infinity);

mongoose
  .connect(
    DATABASE,
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log('Connected to database.');
  });

cron.schedule('*/5 * * * * *', async () => {
  const allProducts = await Product.find({ status: 'unfinished' });

  allProducts.map(async product => {
    if (product.dateTo <= new Date() && product.status === 'unfinished') {
      const firstPrice = await Bid.findById(product.firstPrice);

      if (firstPrice) {
        const advertiser = await Advertiser.findById(firstPrice.advertiserId);
        const secondPrice = await Bid.findById(product.secondPrice);

        if (secondPrice) {
          // tru tien firstPrice voi secondPrice

          await Advertiser.findByIdAndUpdate(
            advertiser._id,
            {
              cash: advertiser.cash - secondPrice.price
            },
            { new: true },
            (error, advertiser) => {
              if (error) throw new Error('Chưa cập nhập được tài khoản!');
              pubsub.publish('advertiserUpdated', advertiser);
            }
          );
        } else {
          // tru tien firstPrice voi startingPrice

          await Advertiser.findByIdAndUpdate(
            advertiser._id,
            {
              cash: advertiser.cash - product.startingPrice
            },
            { new: true },
            (error, advertiser) => {
              if (error) throw new Error('Chưa cập nhập được tài khoản!');
              pubsub.publish('advertiserUpdated', advertiser);
            }
          );
        }
      }

      await Product.findByIdAndUpdate(
        product._id,
        {
          status: 'finished'
        },
        { new: true },
        (err, product) => {
          if (err) throw new Error('Chưa cập nhập được sản phẩm!');
          pubsub.publish('productUpdated', product);
        }
      );
    }
  });
});

const server = new GraphQLServer({
  schema,
  context: req => ({
    ...req
  })
});

server.start(() => console.log('Server is running on localhost:4000'));
