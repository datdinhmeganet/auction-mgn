const jwt = require('jsonwebtoken');
const { BCRYPT_KEY } = require('../config/config');

function getUserId(context) {
  const Authorization = context.request.get('authorization');
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '');
    const { _id } = jwt.verify(token, BCRYPT_KEY);
    return _id;
  }

  throw new Error('Not authenticated');
}

module.exports = getUserId;
