const mongoose = require('mongoose');

const AdvertiserSchema = new mongoose.Schema(
    {
        email: String,
        password: String,
        name: String,
        cash: Number,
    },
);

const Advertiser = mongoose.model('Advertiser', AdvertiserSchema);

module.exports = Advertiser;
