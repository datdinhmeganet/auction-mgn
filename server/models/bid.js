const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const BidSchema = new mongoose.Schema(
    {
        productId: ObjectId,
        advertiserId: ObjectId,
        price: Number,
        createdDate: {
            type: Date,
            default: Date.now
        },
    },
);

const Bid = mongoose.model('Bid', BidSchema);

module.exports = Bid;
