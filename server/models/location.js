const mongoose = require('mongoose');

const LocationSchema = new mongoose.Schema(
    {
        name: String,
        value: Number,
    },
);

const Location = mongoose.model('Location', LocationSchema);

module.exports = Location;
