const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const ProductSchema = new mongoose.Schema(
    {
        locationId: ObjectId,
        startingPrice: Number,
        secondPrice: {
            type: ObjectId,
        },
        firstPrice: {
            type: ObjectId,
        },
        dateFrom: {
            type: Date,
            default: new Date()
        },
        dateTo: {
            type: Date,
            default: new Date().setMinutes(new Date().getMinutes() + 5)
        },
        status: {
            type: String,
            default: 'unfinished'
        }
    },
);

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
