const { composeWithMongoose } = require('graphql-compose-mongoose');
const Product = require('../models/product');
const Admin = require('../models/admin');
const getUserId = require('../utils/getUserId');
const pubsub = require('../utils/subscriptions');

const { GraphQLID, GraphQLNonNull, GraphQLInt } = require('graphql');

const ProductTC = composeWithMongoose(Product);
module.exports = ProductTC;

const LocationTC = require('./locationTC');
const BidTC = require('./bidTC');

ProductTC.addRelation('locationId', {
  resolver: () => LocationTC.getResolver('findById'),
  prepareArgs: {
    _id: source => source.locationId
  },
  projection: { _id: 1 }
});

ProductTC.addRelation('secondPrice', {
  resolver: () => BidTC.getResolver('findById'),
  prepareArgs: {
    _id: source => source.secondPrice
  },
  projection: { _id: 1 }
});

ProductTC.addRelation('firstPrice', {
  resolver: () => BidTC.getResolver('findById'),
  prepareArgs: {
    _id: source => source.firstPrice
  },
  projection: { _id: 1 }
});

ProductTC.addResolver({
  name: 'createProduct',
  type: ProductTC,
  args: {
    dateFrom: 'Date!',
    dateTo: 'Date!',
    locationId: { type: new GraphQLNonNull(GraphQLID) },
    startingPrice: { type: new GraphQLNonNull(GraphQLInt) }
  },
  resolve: async ({ args, context }) => {
    const currentUser = await Admin.findById(getUserId(context));
    if (!currentUser) throw new Error('Tài khoản không hợp lệ!');

    const newProduct = new Product({
      dateFrom: args.dateFrom,
      dateTo: args.dateTo,
      locationId: args.locationId,
      startingPrice: args.startingPrice
    });

    const product = await newProduct.save();

    pubsub.publish('productCreated', product);

    return product;
  }
});
