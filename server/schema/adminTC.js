const { composeWithMongoose } = require('graphql-compose-mongoose');
const jwt = require('jsonwebtoken');
const {
  GraphQLString,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt
} = require('graphql');

const config = require('../config/config');
const Admin = require('../models/admin');
const getUserId = require('../utils/getUserId');

const customizationOptions = {};
const AdminTC = composeWithMongoose(Admin, customizationOptions);

const adminTokenType = new GraphQLObjectType({
  name: 'adminTokenType',
  fields: () => ({
    token: { type: GraphQLString }
  })
});

AdminTC.addResolver({
  name: 'loginAdmin',
  type: adminTokenType,
  args: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve: async ({ args }) => {
    const admin = await Admin.findOne({
      email: args.email.toLowerCase()
    });

    if (!admin) throw new Error('Email không tồn tại!');

    // if (!bcrypt.compareSync(args.password, admin.password)) return null;
    if (args.password !== admin.password)
      throw new Error('Mật khẩu không đúng!');

    const token = await jwt.sign(
      {
        _id: admin._id,
        name: admin.name
      },
      config.BCRYPT_KEY,
      { expiresIn: 86400 }
    );

    return { token };
  }
});

AdminTC.addResolver({
  name: 'findMeAdmin',
  type: AdminTC,
  resolve: async ({ context }) => {
    const admin = Admin.findById(getUserId(context));

    if (!admin) throw new Error('User không tồn tại!');

    return admin;
  }
});

AdminTC.addResolver({
  name: 'topUpCash',
  args: {
    custId: { type: new GraphQLNonNull(GraphQLString) },
    amount: { type: new GraphQLNonNull(GraphQLInt) }
  },
  resolve: async ({ args }) => {
    const currentAdvertiser = await Advertiser.findById(args.custId);

    if (!admin) throw new Error('User không tồn tại!');

    const result = await Advertiser.findByIdAndUpdate(args.custId, {
      cash: currentAdvertiser.cash + args.amount
    });

    if (!result) throw new Error('Cập nhật không thành công!');
  }
});

module.exports = AdminTC;
