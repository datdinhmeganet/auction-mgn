const { schemaComposer } = require('graphql-compose');
const { withFilter } = require('graphql-subscriptions');
const { GraphQLID, GraphQLNonNull } = require('graphql');

const AdvertiserTC = require('./advertiserTC');
const LocationTC = require('./locationTC');
const ProductTC = require('./productTC');
const BidTC = require('./bidTC');
const AdminTC = require('./adminTC');
const Admin = require('../models/admin');
const getUserId = require('../utils/getUserId');
const pubsub = require('../utils/subscriptions');

function authAccess(resolvers) {
  Object.keys(resolvers).forEach(k => {
    resolvers[k] = resolvers[k].wrapResolve(next => rp => {
      // rp = resolveParams = { source, args, context, info }
      if (!getUserId(rp.context))
        throw new Error(
          'You should be logged in, to have access to this action.'
        );

      return next(rp);
    });
  });
  return resolvers;
}

function adminAccess(resolvers) {
  Object.keys(resolvers).forEach(k => {
    resolvers[k] = resolvers[k].wrapResolve(next => async rp => {
      // rp = resolveParams = { source, args, context, info }
      const admin = await Admin.findById(getUserId(rp.context));

      if (!admin)
        throw new Error(
          'You should be an ADMIN, to have access to this action.'
        );

      return next(rp);
    });
  });
  return resolvers;
}

schemaComposer.rootQuery().addFields({
  ...authAccess({
    findManyBid: BidTC.getResolver('findMany'),
    findAdvertiser: AdvertiserTC.getResolver('findMe'),
    findManyLocation: LocationTC.getResolver('findMany'),
    findManyProduct: ProductTC.getResolver('findMany'),
    ...adminAccess({
      findAdmin: AdminTC.getResolver('findMeAdmin'),
      findManyAdmin: AdminTC.getResolver('findMany'),
      findManyAdvertiser: AdvertiserTC.getResolver('findMany')
    })
  })
});

schemaComposer.rootMutation().addFields({
  login: AdvertiserTC.getResolver('login'),
  loginAdmin: AdminTC.getResolver('loginAdmin'),

  ...authAccess({
    createAdvertiser: AdvertiserTC.getResolver('createOne'),
    createBid: BidTC.getResolver('createBid'),

    ...adminAccess({
      createAdmin: AdminTC.getResolver('createOne'),
      createLocation: LocationTC.getResolver('createOne'),
      createProduct: ProductTC.getResolver('createProduct')
    })
  })
});

schemaComposer.rootSubscription().addFields({
  advertiserUpdated: {
    type: AdvertiserTC,
    args: {
      _id: { type: new GraphQLNonNull(GraphQLID) }
    },
    resolve: payload => payload,
    subscribe: withFilter(
      () => pubsub.asyncIterator('advertiserUpdated'),
      (payload, variables) => {
        return String(payload._id) === variables._id;
      }
    )
  },
  productUpdated: {
    type: ProductTC,
    resolve: payload => payload,
    subscribe: () => pubsub.asyncIterator('productUpdated')
  },
  productCreated: {
    type: ProductTC,
    resolve: payload => payload,
    subscribe: () => pubsub.asyncIterator('productCreated')
  }
});

const schema = schemaComposer.buildSchema();

module.exports = schema;
