const { composeWithMongoose } = require('graphql-compose-mongoose');

const Bid = require('../models/bid');
const Advertiser = require('../models/advertiser');
const Product = require('../models/product');
const getUserId = require('../utils/getUserId');
const pubsub = require('../utils/subscriptions');

const { GraphQLID, GraphQLNonNull, GraphQLInt } = require('graphql');

const customizationOptions = {};
const BidTC = composeWithMongoose(Bid, customizationOptions);
module.exports = BidTC;

const AdvertiserTC = require('./advertiserTC');
const ProductTC = require('./productTC');

BidTC.addRelation('productId', {
  resolver: () => ProductTC.getResolver('findById'),
  prepareArgs: {
    _id: source => source.productId
  },
  projection: { _id: 1 }
});

BidTC.addRelation('advertiserId', {
  resolver: () => AdvertiserTC.getResolver('findById'),
  prepareArgs: {
    _id: source => source.advertiserId
  },
  projection: { _id: 1 }
});

BidTC.addResolver({
  name: 'createBid',
  type: BidTC,
  args: {
    productId: { type: new GraphQLNonNull(GraphQLID) },
    price: { type: new GraphQLNonNull(GraphQLInt) }
  },
  resolve: async ({ args, context }) => {
    const currentUser = await Advertiser.findById(getUserId(context));
    if (!currentUser) throw new Error('Tài khoản không hợp lệ!');

    const currentProduct = await Product.findById(args.productId);
    if (!currentProduct) throw new Error('Sản phẩm không hợp lệ!');

    if (currentProduct.dateTo <= new Date()) {
      throw new Error('Đã hết hạn phiên đấu giá!');
    }

    if (currentUser.cash < args.price) {
      throw new Error('Đã hết tiền trong tài khoản!');
    }

    const newBid = new Bid({
      productId: args.productId,
      advertiserId: getUserId(context),
      price: args.price,
      createdDate: new Date().toISOString()
    });

    if (currentProduct.firstPrice) {
      const firstPrice = await Bid.findById(currentProduct.firstPrice);

      if (args.price <= firstPrice.price) {
        throw new Error('Giá đấu thầu phải cao hơn giá hiện tại!');
      } else {
        const bid = await newBid.save();

        await Product.findByIdAndUpdate(
          args.productId,
          {
            firstPrice: bid._id,
            secondPrice: currentProduct.firstPrice
          },
          { new: true },
          (err, product) => {
            if (err) throw new Error('Chưa cập nhập được sản phẩm!');
            pubsub.publish('productUpdated', product);
          }
        );

        return bid;
      }
    } else {
      if (args.price < currentProduct.startingPrice) {
        throw new Error('Giá đấu thầu không được thấp hơn giá khởi điểm!');
      } else {
        const bid = await newBid.save();

        await Product.findByIdAndUpdate(
          args.productId,
          {
            firstPrice: bid._id
          },
          { new: true },
          (err, product) => {
            if (err) throw new Error('Chưa cập nhập được sản phẩm!');
            pubsub.publish('productUpdated', product);
          }
        );

        return bid;
      }
    }
  }
});
