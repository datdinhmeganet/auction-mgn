const { composeWithMongoose } = require('graphql-compose-mongoose');

const Location = require('../models/location');

const customizationOptions = {};

const LocationTC = composeWithMongoose(Location, customizationOptions);

module.exports = LocationTC;