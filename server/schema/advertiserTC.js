const { composeWithMongoose } = require('graphql-compose-mongoose');
const jwt = require('jsonwebtoken');
const { GraphQLString, GraphQLObjectType, GraphQLNonNull } = require('graphql');

const config = require('../config/config');
const Advertiser = require('../models/advertiser');
const getUserId = require('../utils/getUserId');

const customizationOptions = {};
const AdvertiserTC = composeWithMongoose(Advertiser, customizationOptions);

const tokenType = new GraphQLObjectType({
  name: 'tokenType',
  fields: () => ({
    token: { type: GraphQLString }
  })
});

AdvertiserTC.addResolver({
  name: 'login',
  type: tokenType,
  args: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve: async ({ args }) => {
    const advertiser = await Advertiser.findOne({
      email: args.email.toLowerCase()
    });

    if (!advertiser) throw new Error('Email không tồn tại!');

    // if (!bcrypt.compareSync(args.password, advertiser.password)) return null;
    if (args.password !== advertiser.password)
      throw new Error('Mật khẩu không đúng!');

    const token = await jwt.sign(
      {
        _id: advertiser._id,
        name: advertiser.name,
        cash: advertiser.cash
      },
      config.BCRYPT_KEY,
      { expiresIn: 86400 }
    );

    return { token };
  }
});

AdvertiserTC.addResolver({
  name: 'findMe',
  type: AdvertiserTC,
  resolve: async ({ context }) => {
    const advertiser = Advertiser.findById(getUserId(context));

    if (!advertiser) throw new Error('Thông tin không hợp lệ!');

    return advertiser;
  }
});

module.exports = AdvertiserTC;
