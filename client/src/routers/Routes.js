import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import AdminLogin from '../pages/admin/Login/AdminLogin';
import UserLogin from '../pages/user/Login/UserLogin';
import AdminProduct from '../pages/admin/Product/AdminProduct';
import UserProduct from '../pages/user/Product/UserProduct';
import config from '../configs/config';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const token = localStorage.getItem(config.AUTH_TOKEN);

  return (
    <Route
      {...rest}
      render={props =>
        token ? <Component {...props} /> : <Redirect to="/user/login" />
      }
    />
  );
};

const Routes = () => (
  <Switch>
    <Route path="/" exact component={UserLogin} />
    <Route path="/admin/login" exact component={AdminLogin} />
    <Route path="/user/login" exact component={UserLogin} />
    <PrivateRoute path="/admin/product" exact component={AdminProduct} />
    <PrivateRoute path="/user/product" exact component={UserProduct} />
  </Switch>
);

export default Routes;
