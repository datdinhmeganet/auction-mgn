import React, { Component } from 'react';
import gql from 'graphql-tag';
import { withApollo } from 'react-apollo';
import { Container, Row, Col, Input, Button, Card, CardBody } from 'mdbreact';
import { toast } from 'react-toastify';
import { AUTH_TOKEN } from '../configs/config';

const LOGIN_MUTATION = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`;

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };
  }

  componentWillMount() {
    localStorage.getItem(AUTH_TOKEN) && this.props.history.push('/');
  }

  onSubmit = async e => {
    e.preventDefault();

    const { email, password } = this.state;

    const result = await this.props.client.mutate({
      mutation: LOGIN_MUTATION,
      variables: { email, password },
      errorPolicy: 'all'
    });

    if (!result.data.login) {
      toast.error(result.errors[0].message);
    } else {
      const { token } = result.data.login;
      this.saveUserData(token);
      window.location.reload();
    }
  };

  saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token);
  };

  render() {
    return (
      <Container>
        <Row>
          <Col />
          <Col md="4">
            <Card style={{ marginTop: '8em', marginBottom: '8em' }}>
              <CardBody>
                <form onSubmit={e => this.onSubmit(e)}>
                  <div className="grey-text">
                    <Input
                      name="email"
                      label="Email"
                      icon="user"
                      type="email"
                      group
                      validate
                      // error="wrong"
                      // success="right"
                      required
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                    <Input
                      name="password"
                      label="Mật khẩu"
                      icon="lock"
                      type="password"
                      group
                      validate
                      required
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                  </div>
                  <div className="text-center">
                    <Button
                      type="submit"
                      style={{
                        background:
                          'linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%)'
                      }}
                    >
                      Login
                    </Button>
                  </div>
                </form>
              </CardBody>
            </Card>
          </Col>
          <Col />
        </Row>
      </Container>
    );
  }
}

export default withApollo(Login);
