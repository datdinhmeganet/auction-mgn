import React, { Component } from 'react';
import FormLoginUser from '../../../components/Form/FormLoginUser';
import { Grid, Paper, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import logo from '../../../assests/logo.png';
const styles = theme => ({
  header: {
    height: 128,
    textAlign: 'center'
  },
  logo: {
    marginTop: '10%',
    width: 64,
    height: 64
  },
  paper: {
    textAlign: 'center',
    marginTop: '10%'
  }
});
class UserLogin extends Component {
  componentWillMount() {
    const token = localStorage.getItem('auth-token');

    console.log(token)
    if (token) {
      console.log('HERE')
      this.props.history.push('/user/product')
      // window.location.replace('/user/product')
    }

  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <img className={classes.logo} src={logo} alt="logo" />
            <FormLoginUser />
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

UserLogin.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(UserLogin);
