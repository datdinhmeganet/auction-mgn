import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';
import { Container, Card, CardBody, CardHeader } from 'mdbreact';
import InputBid from '../components/InputBid';
import loadingLogo from '../assets/loading-logo.gif';

export const FIND_MANY_PRODUCT_QUERY = gql`
  query {
    findManyProduct {
      _id
      locationId {
        name
        value
      }
      startingPrice
      firstPrice {
        advertiserId {
          name
        }
        price
      }
      dateFrom
      dateTo
      status
    }
  }
`;

const PRODUCT_UPDATED_SUBSCRIPTION = gql`
  subscription {
    productUpdated {
      _id
      locationId {
        name
        value
      }
      startingPrice
      firstPrice {
        advertiserId {
          name
        }
        price
      }
      dateFrom
      dateTo
      status
    }
  }
`;

const PRODUCT_CREATED_SUBSCRIPTION = gql`
  subscription {
    productCreated {
      _id
      locationId {
        name
        value
      }
      startingPrice
      firstPrice {
        advertiserId {
          name
        }
        price
      }
      dateFrom
      dateTo
      status
    }
  }
`;

class Product extends Component {
  locationFormat = cell => {
    return cell ? `${cell.name}` : '';
  };

  valueFormatter = cell => {
    return cell ? cell.value : null;
  };

  dateFormatter = cell => moment(cell).format('HH:mm DD/MM');

  firstPriceFormatter = cell => {
    return cell ? `${cell.advertiserId.name} - ${cell.price}` : '';
  };

  statusFormatter = cell => {
    return cell === 'finished' ? 'Đã xong' : 'Đang GD';
  };

  actionFormatter = (cell, row) => {
    return cell === 'unfinished' ? (
      <InputBid
        startingPrice={row.startingPrice}
        topPrice={row.firstPrice ? row.firstPrice.price : 0}
        productId={row._id}
      />
    ) : null;
  };

  rowClassNameFormat(row) {
    return row.status === 'unfinished'
      ? '#rgba(76, 175, 80, 0.1) rgba-green-slight'
      : '';
  }

  _subscribeToCreateNewProducts = subscribeToMore => {
    subscribeToMore({
      document: PRODUCT_CREATED_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;

        const newProduct = subscriptionData.data.productCreated;

        return Object.assign({}, prev, {
          findManyProduct: [newProduct, ...prev.findManyProduct],
          count: prev.findManyProduct.length + 1,
          __typename: prev.findManyProduct.__typename
        });
      }
    });
  };

  _subscribeToUpdateProducts = subscribeToMore => {
    subscribeToMore({
      document: PRODUCT_UPDATED_SUBSCRIPTION
    });
  };

  render() {
    return (
      <Container style={{ marginTop: '5em' }}>
        <Card>
          <CardHeader>
            <span style={{ color: '#42a5f5', fontSize: 19 }}>
              Dự án đấu thầu
            </span>
          </CardHeader>
          <CardBody>
            <Query query={FIND_MANY_PRODUCT_QUERY}>
              {({ loading, error, data, subscribeToMore }) => {
                if (loading)
                  return <img src={loadingLogo} alt="loading-logo" />;
                if (error) return <div>Error</div>;
                this._subscribeToUpdateProducts(subscribeToMore);
                this._subscribeToCreateNewProducts(subscribeToMore);
                return (
                  <BootstrapTable
                    data={data.findManyProduct}
                    bordered={false}
                    hover
                    pagination
                    options={{
                      noDataText: 'Hiện tại chưa có sản phẩm nào',
                      defaultSortName: 'dateTo',
                      defaultSortOrder: 'desc'
                    }}
                    trClassName={this.rowClassNameFormat}
                  >
                    <TableHeaderColumn
                      dataField="dateFrom"
                      dataSort
                      isKey
                      dataFormat={this.dateFormatter}
                      width="10%"
                    >
                      Từ ngày
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="dateTo"
                      dataSort
                      dataFormat={this.dateFormatter}
                      width="10%"
                    >
                      Đến ngày
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="locationId"
                      dataSort
                      dataFormat={this.locationFormat}
                      width="30%"
                    >
                      Sản phẩm
                    </TableHeaderColumn>
                    {/* <TableHeaderColumn
            dataField="locationId"
            dataSort
            dataFormat={this.valueFormatter}
            width="7%"
          >
            Giá trị
          </TableHeaderColumn> */}
                    <TableHeaderColumn
                      dataField="startingPrice"
                      width="10%"
                      dataSort
                    >
                      Kh.điểm
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="status"
                      dataSort
                      width="10%"
                      dataFormat={this.statusFormatter}
                    >
                      Trạng thái
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="firstPrice"
                      dataSort
                      width="15%"
                      dataFormat={this.firstPriceFormatter}
                    >
                      Thầu hiện tại
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="status"
                      dataSort
                      dataFormat={this.actionFormatter}
                      width="15%"
                    >
                      Hoạt động
                    </TableHeaderColumn>
                  </BootstrapTable>
                );
              }}
            </Query>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

export default Product;
