import React, { Component } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navigation from './components/Navigation';
import Route from './routes/Route';

class App extends Component {
  render() {
    return (
      <div>
        <Navigation />
        <Route />
        <ToastContainer position="bottom-right" autoClose={3000} />
      </div>
    );
  }
}

export default App;
