import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Login from '../pages/Login';
import Product from '../pages/Product';
import { AUTH_TOKEN } from '../configs/config';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const token = localStorage.getItem(AUTH_TOKEN);

  return (
    <Route
      {...rest}
      render={props =>
        token ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

const Routes = () => (
  <Switch>
    <Route path="/login" exact component={Login} />
    <PrivateRoute path="/" exact component={Product} />
  </Switch>
);

export default withRouter(Routes);
