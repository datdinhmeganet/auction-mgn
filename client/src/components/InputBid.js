import React, { Component } from 'react';
import { Input, Button } from 'mdbreact';
import gql from 'graphql-tag';
import { withApollo } from 'react-apollo';
import { toast } from 'react-toastify';

const CREATE_BID_MUTATION = gql`
  mutation CreateBid($productId: ID!, $price: Int!) {
    createBid(productId: $productId, price: $price) {
      productId {
        _id
        locationId {
          name
          value
        }
        startingPrice
        firstPrice {
          advertiserId {
            name
          }
          price
        }
        dateFrom
        dateTo
        status
      }
    }
  }
`;

class InputBid extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      price: null
    };
  }

  getValidationState() {
    const { startingPrice, topPrice } = this.props;
    const { price } = this.state;

    if (price) {
      if (topPrice > 0) {
        if (price > topPrice) {
          return false;
        } else {
          return true;
        }
      } else {
        if (price >= startingPrice) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  onSubmit = async e => {
    e.preventDefault();

    const { productId } = this.props;
    const { price } = this.state;

    const result = await this.props.client.mutate({
      mutation: CREATE_BID_MUTATION,
      variables: { productId, price },
      errorPolicy: 'all'
    });

    if (!result.data.createBid) {
      toast.error(result.errors[0].message);
    } else {
      toast.success('Đấu giá thành công!');
      this.setState({ price: null });
    }
  };

  render() {
    const { price } = this.state;

    return (
      <form onSubmit={e => this.onSubmit(e)}>
        <Input
          type="number"
          name="price"
          label="Giá thầu"
          value={price}
          min={100}
          onChange={e => this.setState({ price: e.target.value })}
        />
        <Button size="sm" type="submit" disabled={this.getValidationState()}>
          OK
        </Button>
      </form>
    );
  }
}

export default withApollo(InputBid);
