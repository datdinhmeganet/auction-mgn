import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import {
  Navbar,
  NavbarBrand,
  Container,
  Collapse,
  NavbarNav,
  NavbarToggler,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu
} from 'mdbreact';
import { toast } from 'react-toastify';
import whiteLogo from '../assets/white-logo.png';
import { AUTH_TOKEN } from '../configs/config';

const FIND_ADVERTISER_QUERY = gql`
  query findAdvertiser {
    findAdvertiser {
      _id
      name
      cash
    }
  }
`;

const ADVERTISER_UPDATED_SUBSCRIPTION = gql`
  subscription AdvertiserUpdated($_id: ID!) {
    advertiserUpdated(_id: $_id) {
      _id
      name
      cash
    }
  }
`;

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false,
      dropdownOpen: false
    };
  }

  onClick() {
    this.setState({
      collapse: !this.state.collapse
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onLogout = () => {
    localStorage.clear();
    window.location.reload();
  };

  _subscribeToUpdateCash = (subscribeToMore, _id) => {
    subscribeToMore({
      document: ADVERTISER_UPDATED_SUBSCRIPTION,
      variables: {
        _id
      }, updateQuery: () => toast.success('Giao dịch thành công!')
    });
  };

  render() {
    const token = localStorage.getItem(AUTH_TOKEN);

    return (
      <Navbar
        dark
        expand="md"
        fixed="top"
        scrolling
        style={{
          background:
            'linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%)'
        }}
      >
        <Container>
          <NavbarBrand href="#">
            <img src={whiteLogo} height="32" width="32" alt="logo" />
          </NavbarBrand>
          {!this.state.isWideEnough && (
            <NavbarToggler onClick={e => this.onClick(e)} />
          )}
          <Collapse isOpen={this.state.collapse} navbar>
            <NavbarNav right>
              {token ? (
                <Query query={FIND_ADVERTISER_QUERY}>
                  {({ loading, error, data, subscribeToMore }) => {
                    if (loading) return <div>Loading</div>;
                    if (error) return <div>Error</div>;
                    const { _id, name, cash } = data.findAdvertiser;
                    this._subscribeToUpdateCash(subscribeToMore, _id);
                    return (
                      <NavItem>
                        <Dropdown>
                          <DropdownToggle nav caret>
                            {`${name} - ${cash.toLocaleString()} ` }<sup>,000 ₫</sup>
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={e => this.onLogout(e)}>
                              Log Out
                            </DropdownItem>
                          </DropdownMenu>
                        </Dropdown>
                      </NavItem>
                    );
                  }}
                </Query>
              ) : null}
            </NavbarNav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default Navigation;
