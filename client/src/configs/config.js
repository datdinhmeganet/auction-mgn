const AUTH_TOKEN = 'meganet';
const API_GRAPHQL_URL = 'http://localhost:4000/graphql';
const API_SUBSCRIPTIONS_URL = 'ws://localhost:4000/subscriptions';

export { AUTH_TOKEN, API_GRAPHQL_URL, API_SUBSCRIPTIONS_URL };
